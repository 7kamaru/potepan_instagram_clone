# Instagram clone

[![MIT License](http://img.shields.io/badge/license-MIT-blue.svg?style=flat)](LICENSE)

Instagramのクローンサイトを目指し制作しました。

## Description

画像を投稿し、他のユーザーと画像をシェアできます。  
気に入ったユーザーをフォロー、画像にいいねできます。

![IMG](app/assets/images/img_instagram_clone.png)

## Features

- Facebookログイン
- 画像投稿
- Follow
- いいね

## Usage

1. Go to [Instagram clone](https://frozen-atoll-42855.herokuapp.com)
2. Example user:  
  ・ Email: 'example@instaclone.com'  
  ・ Password: 'password'

## Installation

    $ git clone https://7kamaru@bitbucket.org/7kamaru/potepan_instagram_clone.git

## Author

[@Nakamaru](https://twitter.com/Nakamar84486715)

## License

This software is released under the MIT License, see LICENSE.txt.