Rails.application.routes.draw do
 
  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
    registrations: 'users/registrations'
  }
  
  root to: 'static_pages#home'
  get '/help', to: 'static_pages#help'
  get '/about', to: 'static_pages#about'
  get '/agreement', to: 'static_pages#agreement'
  get '/image/:id', to: 'images#show'
  
  resources :users do
     member do
      get :following, :followers
    end
  end
  resources :images,        only: [:index, :new, :show, :create, :destroy]
  resources :relationships, only: [:create, :destroy]
  resources :likes,         only: [:create, :destroy]
end
