require 'test_helper'

class ImagesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @image = images(:orange)
    @user = users(:michael)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Image.count' do
      post images_path, params: { images: { content: "Lorem ipsum" } }
    end
    assert_redirected_to new_user_session_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Image.count' do
      delete image_path(@image)
    end
    assert_redirected_to new_user_session_url
  end
  
  test "should redirect destroy for wrong image" do
    log_in_as(users(:michael))
    image = images(:ants)
    assert_no_difference 'Image.count' do
      delete image_path(image)
    end
    assert_redirected_to root_url
  end
end
