require 'test_helper'

class ImageTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:michael)
    @image = @user.images.build(content: "Lorem ipsum")
  end

  test "should be valid" do
    assert @image.valid?
  end

  test "user id should be present" do
    @image.user_id = nil
    assert_not @image.valid?
  end
  
  # test "picture should be present" do
  #   @image.picture = "   "
  #   assert_not @image.valid?
  # end

  test "order should be most recent first" do
    assert_equal images(:most_recent), Image.first
  end
end
