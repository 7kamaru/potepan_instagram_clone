class AddOthersToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :username, :string
    add_column :users, :website, :text
    add_column :users, :introduction, :text
    add_column :users, :tel, :string
    add_column :users, :gender, :string
  end
end
