# ユーザー
User.create!(name:  "Example User",
             username: "Example User",
             email: "example@instaclone.com",
             password:              "password",
             password_confirmation: "password")

99.times do |n|
  name  = Faker::Name.name
  username = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  introduction = Faker::Lorem.sentence(5)
  password = "password"
  User.create!(name:  name,
               username: name,
               email: email,
               introduction: introduction,
               password:              password,
               password_confirmation: password)
end

# イメージ IMG_20190407_162106602_HDR
users = User.order(:created_at).take(6)
50.times do
  # content = Faker::Lorem.sentence(3)
  picture = open("#{Rails.root}/db/fixtures/IMG_20190407_162106602_HDR.jpeg")
  users.each { |user| user.images.create!(picture: picture) }
end



# リレーションシップ
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }