class ImagesController < ApplicationController
  before_action :correct_user, only: :destroy
  
  def index
    @image  = current_user.images.build
    @feed_items = current_user.feed.page params[:page]
  end
  
  def show
    @image = Image.find(params[:id])
  end
  
  def new
    @image = current_user.images.build
  end
  
  def create
    @image = current_user.images.build(image_params)
    if @image.save
      flash[:success] = "写真をアップロードしました。"
      redirect_to image_url(id: @image.id)
    else
      render 'images/new'
    end
  end
  
  def destroy
    @image.destroy
    flash[:success] = "写真を削除しました。"
    redirect_to request.referrer || root_url
  end
  
  private

    def image_params
      params.require(:image).permit(:content, :picture)
    end
    
    def correct_user
      @image = current_user.images.find_by(id: params[:id])
      redirect_to root_url if @image.nil?
    end
end
