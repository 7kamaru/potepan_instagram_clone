class UsersController < ApplicationController
  
  def edit
    @user = User.find(params[:id])
  end
  
  def show
    @user = User.find(params[:id])
    @images = @user.images.page params[:page]
  end
  
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.page params[:page]
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.page params[:page]
    render 'show_follow'
  end
end