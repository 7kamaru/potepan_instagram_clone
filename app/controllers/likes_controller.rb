class LikesController < ApplicationController
  
  def create
    @image = Image.find(params[:image_id])
    unless @image.iine?(current_user)
      @image.iine(current_user)
      respond_to do |format|
        format.html { redirect_to request.referrer || root_url }
        format.js
      end
    end
  end

  def destroy
    @image = Like.find(params[:id]).image
    if @image.iine?(current_user)
      @image.uniine(current_user)
      respond_to do |format|
        format.html { redirect_to request.referrer || root_url }
        format.js
      end
    end
  end
end
