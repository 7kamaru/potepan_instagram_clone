class StaticPagesController < ApplicationController
  before_action :set_password_now, only: :home
  before_action :authenticate_user!, :except=>[:home, :help, :about, :agreement]
  
  def home
  end

  def help
  end
  
  def about
  end
  
  def agreement
  end
  
  private
  
  def set_password_now
    if current_user&.provider == 'facebook' && current_user&.created_at == current_user&.updated_at
      redirect_to edit_user_registration_path
    end
  end
end
