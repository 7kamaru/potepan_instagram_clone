class Image < ApplicationRecord
  has_many :likes, dependent: :destroy
  has_many :iine_users, through: :likes, source: :user
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, length: { maximum: 140 }
  validates :picture, presence: true, if: :content_blank?
  validate  :picture_size
  
  # イメージをいいねする
  def iine(user)
    likes.create(user_id: user.id)
  end

  # イメージのいいねを解除する
  def uniine(user)
    likes.find_by(user_id: user.id).destroy
  end
  
  # 現在のユーザーがいいねしてたらtrueを返す
  def iine?(user)
    iine_users.include?(user)
  end
  
  private

    # アップロードされた画像のサイズをバリデーションする
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "ファイルサイズ上限は5MBです。")
      end
    end
    
    def content_blank?
      content.blank?
    end
end
